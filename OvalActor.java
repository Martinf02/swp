package at.mar.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor{
    private double x,y;

    public OvalActor(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public void update(GameContainer gc, int delta) {
        this.x++;
    }

    public void render(Graphics graphics) {
        graphics.drawOval((float)this.x, (float)this.y, 30, 50);
        //(float) ist dafür da, den Datentyp double in float umzuwandeln
    }

}
