package at.mar.games.wintergame;

import org.newdawn.slick.*;
import org.newdawn.slick.tests.AnimationTest;
import org.w3c.dom.css.Rect;

import java.util.ArrayList;
import java.util.List;

public class MainGame extends BasicGame {
    private CircleActor ca1, ca2;
    private RectActor ra1, ra2;
    private OvalActor oa1;
    private List<Actor> actors;


    public MainGame(String title) {
        super(title);
    }
    int x;
    int y;

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        //Wird dazu verwendet Spiele aufzubauen, wird nur 1 mal aufgerufen
        this.x = 100;
        this.x = 100;       //Ein Rechteck wird erstellt und an den Positionen 100 wird gestartet

        this.ca1 = new CircleActor(100,100);
        this.ca2 = new CircleActor(200,300);
        this.ra1 = new RectActor(50,50);
        this.ra2 = new RectActor(100,100);
        this.oa1 = new OvalActor(50,50);

        this.actors = new ArrayList<>();
        this.actors.add(new CircleActor(350,100));
        this.actors.add(new RectActor(350,100));
        this.actors.add(new OvalActor(350,100));
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        //Wird laufend, ganze Zeit aufgerufen
        //delta = Zeit seit dem letzten Aufruf
        this.x++;           //Das Rechteck fliegt dauerhaft nach rechts unten
        this.ca1.update(gc, delta);
        this.ca2.update(gc, delta);
        this.ra1.update(gc,delta);
        this.ra2.update(gc,delta);
        this.oa1.update(gc,delta);

        for (Actor actor : actors)
        {
            actor.update(gc, delta);
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        //Wird nur aufgerufen, wenn es notwendig ist, hier wird gezeichnet
        graphics.drawRect(this.x, this.y,50,50 );       //Die Simulation wird gezeichnet

        this.ca1.render(graphics);
        this.ca2.render(graphics);
        this.ra1.render(graphics);
        this.ra2.render(graphics);
        this.oa1.render(graphics);

        for (Actor actor : actors)
        {
            actor.render(graphics);
        }
    }



    public static void main(String[] args) {
        try {
            AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
            container.setDisplayMode(1000,800,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
